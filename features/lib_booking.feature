Feature: Open Library Book Borrowing System
  As a customer 
  I need open System
  Such that I can see book details for extension
  I want to extention for book

  Scenario: Book loan extension
    Given Select Book for extension
          | Title                          | Code  | Start date  | Due date   | Times extended| Action
          | Programming Phoenix 1.3        | A     | 09-01-2019  | 16-01-2019 | 0             | Extend |
          | Secret of the JavaScript ninja | A     | 19-12-2018  | 16-01-2019 | 4             | Extend |
    And I want to extention of "7" days for "Programming Phoenix 1.3"
    And I click on select Bullet
    When I summit the extention request
    Then I should receive a confirmation message

    Scenario: Reading list
    Given Check times extended
          | Title                          | Code  | Start date  | Due date   | Times extended| Action
          | Programming Phoenix 1.3        | A     | 09-01-2019  | 16-01-2019 | 1             | Extend |
          | Secret of the JavaScript ninja | A     | 19-12-2018  | 16-01-2019 | 4             | Extend |
    And I want to see of "Times extended" for "Programming Phoenix 1.3"
    And I click on select Bullet
    When I summit the extention request
    Then I should see a number of extention