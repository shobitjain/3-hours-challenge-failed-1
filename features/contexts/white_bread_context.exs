defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers
  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    %{}
  end
  scenario_finalize fn _status, _state ->
    #Hound.end_session
    nil
  end


  given_ ~r/^Check times extended$/, fn state ->
    navigate_to "/booking/index"
    {:ok, state}
  end
  and_ ~r/^I want to see of "(?<argument_one>[^"]+)" for "(?<argument_two>[^"]+)"$/,
  fn state, %{argument_one: _argument_one,argument_two: _argument_two} ->
  {:ok, state}
  end
  and_ ~r/^I click on select Bullet$/, fn state ->
    click({:id, "1"})
    {:ok, state}
  end
  when_ ~r/^I summit the extention request$/, fn state ->
    {:ok, state}
  end
  then_ ~r/^I should see a number of extention$/, fn state ->
    {:ok, state}
  end
  given_ ~r/^Select Book for extension$/, fn state ->
    {:ok, state}
  end
  and_ ~r/^I want to extention of "(?<argument_one>[^"]+)" days for "(?<argument_two>[^"]+)"$/,
  fn state, %{argument_one: _argument_one,argument_two: _argument_two} ->
    {:ok, state}
  end
  then_ ~r/^I should receive a confirmation message$/, fn state ->
    {:ok, state}
  end
end
