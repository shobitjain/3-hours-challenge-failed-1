defmodule Exam.Repo.Migrations.Extention do
  use Ecto.Migration

  def change do
    create table(:extention) do
      add :title, :string
      add :code, :string
      add :start_date, :string
      add :due_date, :string
      add :times_extended, :string
      add :action, :string
      timestamps()
    end
  end
end
