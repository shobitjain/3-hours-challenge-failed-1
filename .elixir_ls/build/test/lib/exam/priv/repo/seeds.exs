# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Exam.Repo.insert!(%Exam.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias exam.{Repo, Book.Extention}

[%{title: "Programming Phoenix 1.3", code: "A", start_date: "09-01-2019", due_date: "16-01-2019", times_extended: "0", action: "Extend"},
 %{title: "Secret of the JavaScript ninja", code: "A", start_date: "19-12-2018", due_date: "16-01-2019", times_extended: "4", action: "Extend"}]

|> Enum.map(fn extention_data -> Extention.changeset(%Extention{}, extention_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)
