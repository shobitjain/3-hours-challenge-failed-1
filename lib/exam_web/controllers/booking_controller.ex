defmodule ExamWeb.BookingController do
  use ExamWeb, :controller
  import Ecto.Query, only: [from: 2]
  alias Exam.Repo
  alias Exam.Exam, as: EXM

  def index(conn, _params) do
    query= from t in EXM, select: t
    exam = Repo.all(query)
    render conn, "index.html", exam: exam
  end
end
