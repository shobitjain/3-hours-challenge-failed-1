defmodule Exam.Book.Extention do
  use Ecto.Schema
  import Ecto.Changeset


  schema "extention" do
    field :title, :string
    field :code, :string
    field :start_date, :string
    field :due_date, :string
    field :times_extended, :string
    field :action, :string
    timestamps()
  end

  @doc false
  def changeset(extention, attrs) do
    extention
    |> cast(attrs, [:title, :code, :start_date, :due_date, :times_extended, :action])
    |> validate_required([:title, :code, :start_date, :due_date, :times_extended, :action])
  end
end
