defmodule ExamWeb.LibbookControllerTest do
  use ExamWeb.ConnCase

  test "matching current date with due date", %{conn: conn} do
    validate={due_date: "16-01-2019"}}
    [{:ok, user1}] = Enum.map(extention, &Accounts.create_user(&1))
    sql = "select due_date from extension;"
    case Postgrex.Connection.query([due_date]) do
    {:error, err} -> {:error, err}
  end

  test "update of due date", %{conn: conn} do
    validate={due_date: "16-01-2019"}}
    [{:ok, user1}] = Enum.map(extention, &Accounts.create_user(&1))
    sql = "upsert into extention(due_date) values(23-01-2019);"
    case Postgrex.Connection.query([due_date]) do
    {:error, err} -> {:error, err}
  end

end
